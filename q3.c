#include <stdio.h>
 int main(){
  int m, n, p, q, c, d, k, sum = 0;
  int f[10][10], s[10][10], multiply[10][10];
  printf("Number of rows and columns in the first matrix: ");
  scanf("%d%d", &m, &n);
  printf("Elements in the first matrix: ");
 
  for (c = 0; c < m; c++)
    for (d = 0; d < n; d++)
      scanf("%d", &f[c][d]);
 
  printf("Number of rows and columns in the second matrix: ");
  scanf("%d%d", &p, &q);
 
  if (n != p)
    printf("Error\n");
  else
  {
    printf("Elements in the second matrix: ");
 
    for (c = 0; c < p; c++)
      for (d = 0; d < q; d++)
        scanf("%d", &s[c][d]);
 
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          sum = sum + f[c][k]*s[k][d];
        }
        multiply[c][d] = sum;
        sum = 0;
      }
    }
    printf("Result: \n");
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++)
        printf("%d\t", multiply[c][d]);
      printf("\n");
    }
  }
  return 0;
}
