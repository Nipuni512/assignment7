#include<stdio.h>
#define size 50                                  

int main(){  
    int count=0;
    char ch, arr[size] = {'\0'}; 
    printf("String: ");
    fgets(arr, sizeof(arr), stdin);
    printf("Character: ");
    scanf("%c", &ch);
    for(int i=0; arr[i] != '\0'; i++)
        if(ch == arr[i]) count++;
    printf("\nFrequency of '%c' is %d.", ch, count);
    return 0;   
}
